package com.f1k375.dotawiki.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.f1k375.dotawiki.HeroesDota
import com.f1k375.dotawiki.R
import com.f1k375.dotawiki.activities.HeroDetailActivity
import com.f1k375.dotawiki.activities.HeroDetailActivity.Companion.ID_HERO
import com.f1k375.dotawiki.databinding.RawItemListBinding

class HeroesAdapter(
    private val mData: List<HeroesDota>,
) : RecyclerView.Adapter<HeroesAdapter.ListViewHolder>() {


    inner class ListViewHolder(@NonNull private val mBinding: RawItemListBinding) :
        RecyclerView.ViewHolder(mBinding.root) {

        private val mContext = mBinding.root.context

        fun bind(hero: HeroesDota) {
            mBinding.run {
                mTextName.text = hero.mName
                mPictHero.setImageResource(hero.mPicture)
                mTextClass.text = hero.mClass + " Hero"
                mTextRole.text = hero.mRole

                mTextClass.setCompoundDrawablesRelativeWithIntrinsicBounds(when (hero.mClass) {
                    "Strength" -> R.drawable.icon_strength
                    "Agility" -> R.drawable.icon_agility
                    "Intelligence" -> R.drawable.icon_intelligence
                    else -> R.drawable.icon_intelligence
                },0,0,0)

                mCardHero.setOnClickListener {
                    mContext.startActivity(
                        Intent(mContext, HeroDetailActivity::class.java).putExtra(
                            ID_HERO,
                            hero.mCode
                        )
                    )
                }
            }
        }
    }

    @NonNull
    override fun onCreateViewHolder(@NonNull mParent: ViewGroup, mViewType: Int): ListViewHolder {
        return ListViewHolder(
            RawItemListBinding.inflate(
                LayoutInflater.from(mParent.context), mParent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    override fun onBindViewHolder(mHolder: ListViewHolder, mPosition: Int) {
        mHolder.bind(mData[mPosition])
    }
}