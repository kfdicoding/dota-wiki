package com.f1k375.dotawiki.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.f1k375.dotawiki.R
import com.f1k375.dotawiki.databinding.ActivityMainBinding
import com.f1k375.dotawiki.utils.viewBinding

class MainActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityMainBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setUpBottomNav()
    }

    private fun setUpBottomNav(){
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.mContainerNavHost) as NavHostFragment
        val navController = navHostFragment.navController
        binding.mBottomNav.setupWithNavController(navController)
    }
}