package com.f1k375.dotawiki.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.AppCompatImageView
import com.f1k375.dotawiki.R
import com.f1k375.dotawiki.data.DataDotaHeroes
import com.f1k375.dotawiki.databinding.ActivityHeroDetailBinding
import com.f1k375.dotawiki.helpers.SharedPreferences
import com.f1k375.dotawiki.utils.viewBinding

class HeroDetailActivity : AppCompatActivity() {

    companion object{
        const val ID_HERO = "id_hero"
    }

    private val binding by viewBinding(ActivityHeroDetailBinding::inflate)
    private val mSharedHelper by lazy { SharedPreferences(this) }

    private fun isFavorite(code: String): Boolean {
        return code in mSharedHelper.getFavoriteHero()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setView()
    }

    private fun setView(){
        val mHeroDetail = DataDotaHeroes.mGetHero(mID = intent.getStringExtra(ID_HERO) ?: "")
        setButtonFav(mHeroDetail.mCode)

        binding.run {
            mBackButton.setOnClickListener {
                onBackPressed()
            }

            mImageHero.setImageResource(mHeroDetail.mPicture)
            mHeroName.text = mHeroDetail.mName

            mTextSTR.text = mHeroDetail.mAttributes?.mSTR
            mTextAGT.text = mHeroDetail.mAttributes?.mAGT
            mTextINT.text = mHeroDetail.mAttributes?.mINT

            mTextClass.text = mHeroDetail.mClass
            mTextRole.text = mHeroDetail.mRole

            mTextMoveSpeed.text = mHeroDetail.mAttributes?.mBaseMoveSpeed.toString()
            mTextAtkSpeed.text = mHeroDetail.mAttributes?.mBaseAtkSpeed.toString()
            mTextArmor.text = mHeroDetail.mAttributes?.mBaseArmor.toString()
            mTextDamage.text = mHeroDetail.mAttributes?.mBaseDamage
            mTextRange.text = mHeroDetail.mAttributes?.mAtkRange.toString()

            mTextLore.text = mHeroDetail.mLore
        }
    }

    private fun setButtonFav(mCode: String) {
        binding.buttonFav.run {
            setImageResource(
                if (isFavorite(mCode)) R.drawable.ic_favorite
                else R.drawable.ic_favorite_border
            )

            setOnClickListener {
                if (isFavorite(mCode))
                    removeFromFavorite(this, mCode)
                else
                    addToFavorite(this, mCode)
            }
        }
    }

    private fun addToFavorite(appCompatImageView: AppCompatImageView, mCode: String) {
        appCompatImageView.setImageResource(R.drawable.ic_favorite)
        mSharedHelper.addFavoriteHero(mCode)
    }

    private fun removeFromFavorite(appCompatImageView: AppCompatImageView, mCode: String) {
        appCompatImageView.setImageResource(R.drawable.ic_favorite_border)
        mSharedHelper.removeFavoriteHero(mCode)
    }
}
