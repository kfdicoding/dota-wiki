package com.f1k375.dotawiki.helpers

import android.content.Context
import androidx.core.content.edit

class SharedPreferences(
    private val context: Context
) {

    companion object{
        private const val DATA_FAVORITE = "sharedDataFavorite"
    }

    private val mSharedPreferences by lazy { context.getSharedPreferences("AppShared", Context.MODE_PRIVATE) }

    fun addFavoriteHero(codeHero: String){
        val currentData = ArrayList<String>().apply {
            addAll(getFavoriteHero())
        }
        currentData.add(codeHero)
        saveFavorite(currentData)
    }

    fun removeFavoriteHero(codeHero: String){
        val currentData = ArrayList<String>().apply {
            addAll(getFavoriteHero())
        }
        currentData.remove(codeHero)
        saveFavorite(currentData)
    }

    fun getFavoriteHero(): List<String>{
        val rawData = mSharedPreferences.getString(DATA_FAVORITE, "") ?: ""
        return rawData.split(",").filter { it.isNotEmpty() }
    }

    private fun saveFavorite(data: List<String>){
        val dataToString = data.joinToString(",").replace(" ","")
        mSharedPreferences.edit {
            putString(DATA_FAVORITE, dataToString)
        }
    }

}