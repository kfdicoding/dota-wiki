package com.f1k375.dotawiki.data

import com.f1k375.dotawiki.AttributesHeroes
import com.f1k375.dotawiki.HeroesDota
import com.f1k375.dotawiki.R
import com.f1k375.dotawiki.data.DataDotaHeroes.mHeroesID
import com.f1k375.dotawiki.utils.getMemberFloat
import com.f1k375.dotawiki.utils.getMemberInt
import com.f1k375.dotawiki.utils.getMemberString
import com.google.gson.JsonArray
import com.google.gson.JsonParser

object DataDotaHeroes {

    private val mHeroesID = arrayOf(
        "AB", "AL", "Ax", "AM", "AW", "MP", "IN", "PK", "RB", "PG"
    )

    private val mHeroesName = arrayOf(
        "Abaddon",
        "Alchemist",
        "Axe",
        "Anti-Mage",
        "Arc Warden",
        "Meepo",
        "Invoker",
        "Puck",
        "Rubick",
        "Pudge"
    )

    private val mHeroesClass = arrayOf(
        "Strength",
        "Strength",
        "Strength",
        "Agility",
        "Agility",
        "Agility",
        "Intelligence",
        "Intelligence",
        "Intelligence",
        "Strength"
    )

    private val mHeroesPic = arrayOf(
        R.drawable.icon_abaddon,
        R.drawable.icon_alchemist,
        R.drawable.icon_axe,
        R.drawable.icon_antimage,
        R.drawable.icon_arcwarden,
        R.drawable.icon_meepo,
        R.drawable.icon_invoker,
        R.drawable.icon_puck,
        R.drawable.icon_rubick,
        R.drawable.icon_pudge
    )

    private val mHeroesRole = arrayOf(
        "Melee, Carry, Durable, Support",
        "Melee, Carry, Disabler, Durable, Initiator, Nuker, Support",
        "Melee, Disabler, Durable, Initiator, Jungler",
        "Melee, Carry, Escape, Nuker",
        "Ranged, Carry, Escape, Nuker",
        "Melee, Carry, Disabler, Escape, Initiator, Nuker, Pusher",
        "Ranged, Carry, Disabler, Escape, Nuker, Pusher",
        "Ranged, Disabler, Escape, Initiator, Nuker",
        "Ranged, Disabler, Nuker, Support",
        "Melee, Disabler, Durable, Initiator, Nuker"
    )

    private val mHeroesLore = arrayOf(
        """
            The Font of Avernus is the source of a family's strength,
            a crack in primal stones from which vapors of prophetic power have issued for generations.
            Each newborn of the cavernous House Avernus is bathed in the black mist,
            and by this baptism they are given an innate connection to the mystic energies of the land.
            They grow up believing themselves fierce protectors of their lineal traditions,
            the customs of the realm--but what they really are protecting is the Font itself.
            And the motives of the mist are unclear.
        """.trimIndent(),
        """
            The sacred science of Chymistry was a Darkbrew family tradition,
            but no Darkbrew had ever shown the kind of creativity, ambition, and recklessness of young Razzil.
            However, when adulthood came calling he pushed aside the family trade to try his hand at manufacturing gold through Alchemy.
            In an act of audacity befitting his reputation, Razzil announced he would transmute an entire mountain into gold. 
        """.trimIndent(),
        """
            As a grunt in the Army of Red Mist, Mogul Khan set his sights on the rank of Red Mist General.
            In battle after battle he proved his worth through gory deed.
            His rise through the ranks was helped by the fact that he never hesitated to decapitate a superior.
            Through the seven year Campaign of the Thousand Tarns, he distinguished himself in glorious carnage,
            his star of fame shining ever brighter, while the number of comrades in arms steadily dwindled.
            On the night of ultimate victory, Axe declared himself the new Red Mist General, and took on the ultimate title of 'Axe.'
            But his troops now numbered zero. Of course, many had died in battle, but a significant number had also fallen to Axe's blade.
            Needless to say, most soldiers now shun his leadership. But this matters not a whit to Axe, who knows that a one-man army is by far the best.
        """.trimIndent(),
        """
            The monks of Turstarkuri watched the rugged valleys below their mountain monastery as wave after wave of invaders swept through the lower kingdoms.
            Ascetic and pragmatic, in their remote monastic eyrie they remained aloof from mundane strife, wrapped in meditation that knew no gods or elements of magic.
            Then came the Legion of the Dead God, crusaders with a sinister mandate to replace all local worship with their Unliving Lord's poisonous nihilosophy.
        """.trimIndent(),
        """
             Before the beginning of all, there existed a presence: a primordial mind, infinite, awesome, and set to inscrutable purpose.
             As the universe thundered into being, this mind was fragmented and scattered.
             Two among its greater fragments--who would come to be named Radiant and Dire--found themselves locked in vicious opposition,
             and began twisting all of creation to serve their conflict.
        """.trimIndent(),
        """
            If you ask me, life is all about who you know and what you can find. When you live up in the Riftshadow Ruins, just finding food can be tough.
            So you need to cut corners, you need to scrounge, you need to know your strengths. Some of the beasts up there can kill you,
            so you need a way to trap the weak and duck the strong. On the upside, the ruins have history, and history is worth a lot to some people.
            There used to be a palace there, where they had all these dark rituals.
        """.trimIndent(),
        """
             In its earliest, and some would say most potent form, magic was primarily the art of memory.
             It required no technology, no wands or appurtenances other than the mind of the magician.
             All the trappings of ritual were merely mnemonic devices, meant to allow the practitioner to recall in rich detail the specific mental formulae
             that unlocked a spell's power. The greatest mages in those days were the ones blessed with the greatest memories,
             and yet so complex were the invocations that all wizards were forced to specialize.
             The most devoted might hope in a lifetime to have adequate recollection of three spells—four at most.
        """.trimIndent(),
        """
             While Puck seems at first glance a mischievous, childish character, this quality masks an alien personality.
             The juvenile form of a Faerie Dragon, a creature that lives for eons, Puck spends countless millennia in its childish form.
             So while it is technically true that Puck is juvenile, it will continue to be so when the cities of the present age have sloughed away into dust.
             Its motives are therefore inscrutable, and what appears to be play may in fact indicate a darker purpose or just its endless fondness for mischief.
        """.trimIndent(),
        """
            Any mage can cast a spell or two, and a few may even study long enough to become a wizard,
            but only the most talented are allowed to be recognized as a Magus.
            Yet as with any sorcerer’s circle, a sense of community has never guaranteed competitive courtesy.
        """.trimIndent(),
        """
            In the Fields of Endless Carnage, far to the south of Quoidge, a corpulent figure works tirelessly through the night--dismembering,
            disembowelling, piling up the limbs and viscera of the fallen that the battlefield might be clear by dawn. In this cursed realm,
            nothing can decay or decompose; no corpse may ever return to the earth from which it sprang, no matter how deep you dig the grave.
            Flocked by carrion birds who need him to cut their meals into beak-sized chunks,
            Pudge the Butcher hones his skills with blades that grow sharper the longer he uses them.
        """.trimIndent()
    )

    private val mHeroesAttribute = JsonParser().parse(
        """[
        {
            "MoveSpeed" : 325,
            "BaseArmor" : 2.22,
            "BaseAtkSpeed" : 1.7,
            "BaseDamage" : "51 - 61",
            "AtkRange" : 150,
            "strength" : "23 + 3",
            "agility" : "23 + 1.5",
            "intelligence" : "18 + 2"
        },
        {
            "MoveSpeed" : 305,
            "BaseArmor" : 2.08,
            "BaseAtkSpeed" : 1.7,
            "BaseDamage" : "49 - 58",
            "AtkRange" : 150,
            "strength" : "25 + 2.7",
            "agility" : "22 + 1.5",
            "intelligence" : "25 + 1.8"
        },
        {
            "MoveSpeed" : 310,
            "BaseArmor" : 1.8,
            "BaseAtkSpeed" : 1.7,
            "BaseDamage" : "52 - 56",
            "AtkRange" : 150,
            "strength" : "25 + 3.6",
            "agility" : "20 + 2.2",
            "intelligence" : "18 + 1.6"
        },
        {
            "MoveSpeed" : 310,
            "BaseArmor" : 2.38,
            "BaseAtkSpeed" : 1.4,
            "BaseDamage" : "52 - 57",
            "AtkRange" : 150,
            "strength" : "23 + 1.3",
            "agility" : "24 + 3.0",
            "intelligence" : "12 + 1.8"
        },
        {
            "MoveSpeed" : 285,
            "BaseArmor" : 0.1,
            "BaseAtkSpeed" : 1.7,
            "BaseDamage" : "46 - 56",
            "AtkRange" : 625,
            "strength" : "25 + 3.0",
            "agility" : "15 + 2.4",
            "intelligence" : "24 + 2.6"
        },
        {
            "MoveSpeed" : 330,
            "BaseArmor" : 6.38,
            "BaseAtkSpeed" : 1.7,
            "BaseDamage" : "46 - 52",
            "AtkRange" : 150,
            "strength" : "24 + 1.8",
            "agility" : "17 + 1.8",
            "intelligence" : "20 + 1.6"
        },
        {
            "MoveSpeed" : 280,
            "BaseArmor" : 1.96,
            "BaseAtkSpeed" : 1.7,
            "BaseDamage" : "42 - 48",
            "AtkRange" : 600,
            "strength" : "18 + 2.4",
            "agility" : "14 + 1.9",
            "intelligence" : "15 + 4.6"
        },
        {
            "MoveSpeed" : 290,
            "BaseArmor" : 0.08,
            "BaseAtkSpeed" : 1.7,
            "BaseDamage" : "46 - 57",
            "AtkRange" : 550,
            "strength" : "17 + 2.4",
            "agility" : "22 + 2.2",
            "intelligence" : "23 + 3.5"
        },
        {
            "MoveSpeed" : 290,
            "BaseArmor" : 3.22,
            "BaseAtkSpeed" : 1.7,
            "BaseDamage" : "47 - 57",
            "AtkRange" : 550,
            "strength" : "21 + 2.0",
            "agility" : "23 + 2.5",
            "intelligence" : "25 + 3.1"
        },
        {
            "MoveSpeed" : 280,
            "BaseArmor" : -0.04,
            "BaseAtkSpeed" : 1.7,
            "BaseDamage" : "65 - 71",
            "AtkRange" : 150,
            "strength" : "25 + 4.0",
            "agility" : "14 + 1.5",
            "intelligence" : "16 + 1.5"
        }
        ]""".trimIndent()
    ) as JsonArray

    private fun heroesAttributes(index: Int): AttributesHeroes{
        val mGetHeroesAttribute = mHeroesAttribute[index].asJsonObject
        return AttributesHeroes().apply {
            mBaseMoveSpeed = mGetHeroesAttribute.getMemberInt("MoveSpeed")
            mBaseArmor = mGetHeroesAttribute.getMemberString("BaseArmor")
            mBaseAtkSpeed = mGetHeroesAttribute.getMemberFloat("BaseAtkSpeed")
            mBaseDamage = mGetHeroesAttribute.getMemberString("BaseDamage")
            mAtkRange = mGetHeroesAttribute.getMemberInt("AtkRange")
            mSTR = mGetHeroesAttribute.getMemberString("strength")
            mAGT = mGetHeroesAttribute.getMemberString("agility")
            mINT = mGetHeroesAttribute.getMemberString("intelligence")
        }
    }

    fun mGetHero(index: Int = -1, mID: String = ""): HeroesDota {
        val mPosition = if (mID.isNotEmpty()) mHeroesID.indexOf(mID) else index

        if (mPosition == -1) return HeroesDota()

        val mHeroes = HeroesDota().apply {
            mCode = mHeroesID[mPosition]
            mName = mHeroesName[mPosition]
            mPicture = mHeroesPic[mPosition]
            mClass = mHeroesClass[mPosition]
            mRole = mHeroesRole[mPosition]
            mLore = mHeroesLore[mPosition]
        }

        mHeroes.mAttributes = heroesAttributes(mPosition)

        return mHeroes
    }

    val mGenerateListHeroes: ArrayList<HeroesDota>
        get() {
            val mList = ArrayList<HeroesDota>()
            mHeroesName.forEachIndexed { index, _ ->
                mList.add(mGetHero(index))
            }
            return mList
        }
}