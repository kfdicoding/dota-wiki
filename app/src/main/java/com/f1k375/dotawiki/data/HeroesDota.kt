package com.f1k375.dotawiki

data class HeroesDota(
    var mCode: String = "",
    var mName:String = "None",
    var mPicture: Int = 0,
    var mClass:String = "None",
    var mRole:String = "None",
    var mLore:String = "None",
    var mAttributes: AttributesHeroes? = null
)

data class AttributesHeroes(
    var mBaseMoveSpeed: Int = 0,
    var mBaseArmor: String = "",
    var mBaseAtkSpeed: Float = 0f,
    var mBaseDamage: String = "",
    var mAtkRange: Int = 0,
    var mSTR: String = "",
    var mINT: String = "",
    var mAGT: String = ""
)