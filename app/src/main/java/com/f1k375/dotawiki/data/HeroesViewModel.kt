package com.f1k375.dotawiki.data

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.f1k375.dotawiki.HeroesDota
import com.f1k375.dotawiki.helpers.SharedPreferences

class HeroesViewModel(application: Application) : AndroidViewModel(application) {

    /**
     * memenag disengaja untuk penyimpanan data favorite menggunakan sharedpreference
     */

    private val mSharedHelper by lazy { SharedPreferences(getApplication()) }

    private val _favoriteHeroes = MutableLiveData<List<HeroesDota>>()
    val listFavoriteHeroes : LiveData<List<HeroesDota>>
    get() = _favoriteHeroes


    private val allHeroes = DataDotaHeroes.mGenerateListHeroes
    private val codeFav
    get() = mSharedHelper.getFavoriteHero()

    fun getHeroes(){
        _favoriteHeroes.postValue(
            allHeroes.filter { it.mCode in codeFav }
        )
    }

    fun addFavorite(code: String){
        mSharedHelper.addFavoriteHero(code)
        getHeroes()
    }

    fun deleteFavorite(code: String){
        mSharedHelper.removeFavoriteHero(code)
        getHeroes()
    }

}