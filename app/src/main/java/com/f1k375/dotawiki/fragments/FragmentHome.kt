package com.f1k375.dotawiki.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.f1k375.dotawiki.R
import com.f1k375.dotawiki.adapters.HeroesAdapter
import com.f1k375.dotawiki.data.DataDotaHeroes
import com.f1k375.dotawiki.databinding.FragmentHomeBinding
import com.f1k375.dotawiki.utils.viewBinding

class FragmentHome : Fragment(R.layout.fragment_home) {

    private val binding by viewBinding(FragmentHomeBinding::bind)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.run {
            mListWiki.hasFixedSize()
            mListWiki.adapter = HeroesAdapter(DataDotaHeroes.mGenerateListHeroes)
        }
    }
}
