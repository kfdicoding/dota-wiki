package com.f1k375.dotawiki.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.f1k375.dotawiki.HeroesDota
import com.f1k375.dotawiki.R
import com.f1k375.dotawiki.adapters.HeroesAdapter
import com.f1k375.dotawiki.data.HeroesViewModel
import com.f1k375.dotawiki.databinding.FragmentFavoriteBinding
import com.f1k375.dotawiki.utils.viewBinding

class FragmentFavorite: Fragment(R.layout.fragment_favorite) {

    private val binding by viewBinding(FragmentFavoriteBinding::bind)
    private val model: HeroesViewModel by activityViewModels()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setView()
        setObserver()
    }

    private fun setView(){
        binding.run {
            listFavorite.hasFixedSize()
        }
    }

    private fun setObserver(){
        model.listFavoriteHeroes.observe(viewLifecycleOwner){result->
            setUpListHero(result)
        }
    }

    private fun setUpListHero(result: List<HeroesDota>) {
        binding.listFavorite.adapter = HeroesAdapter(result)
    }

    override fun onResume() {
        super.onResume()
        model.getHeroes()
    }

}