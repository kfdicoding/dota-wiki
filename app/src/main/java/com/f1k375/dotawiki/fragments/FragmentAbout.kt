package com.f1k375.dotawiki.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.f1k375.dotawiki.R

class FragmentAbout : Fragment(R.layout.fragment_about) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

}
