package com.f1k375.dotawiki.utils

import com.google.gson.JsonObject

fun JsonObject.getMemberInt(key: String): Int{
    return getAsJsonPrimitive(key).asInt
}

fun JsonObject.getMemberString(key: String): String{
    return getAsJsonPrimitive(key).asString
}

fun JsonObject.getMemberFloat(key: String): Float{
    return getAsJsonPrimitive(key).asFloat
}